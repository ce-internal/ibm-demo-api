package ibm.com.ibmdemoapi.repository;

import ibm.com.ibmdemoapi.entity.CovidReport;
import org.springframework.data.repository.CrudRepository;

public interface CovidReportRepository extends CrudRepository<CovidReport, Long> {
}
