package ibm.com.ibmdemoapi.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import ibm.com.ibmdemoapi.entity.CovidReport;
import ibm.com.ibmdemoapi.repository.CovidReportRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;
import java.util.Optional;

@RestController
@RequestMapping(path="/covidReport")
public class CovidReportController {

    private static final Logger logger = LogManager.getLogger(CovidReportController.class);

    @Autowired
    private CovidReportRepository covidReportRepository;

    private static final ObjectMapper objectMapper = new ObjectMapper();

    @RequestMapping(value="/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String getAllCovidReports() {
        try {
            logger.info("Get all covid reports");
            return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(covidReportRepository.findAll());
        }
        catch (JsonProcessingException e){
            logger.error("Exception occur ", e);
            return e.toString();
        }
    }

    @RequestMapping(value="", method = RequestMethod.GET)
    public String getCovidReport(@RequestParam(value = "id") Long id){
        try {
            Optional<CovidReport> covidReportOptional = covidReportRepository.findById(id);
            if (covidReportOptional.isPresent()) {
                String result = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(covidReportRepository.findById(id).get());
                logger.info(String.format("Get covid report %d %s", id, result));
                return result;
            } else {
                logger.warn(String.format("The covid report %d is not exist!", id));
                return String.format("The covid report %d is not exist!", id);
            }
        } catch (JsonProcessingException e){
            logger.error("Exception occur ", e);
            return e.toString();
        }
    }

    @RequestMapping(value="", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.CREATED, reason = "OK")
    public String createCovidReport(@RequestBody CovidReport covidReport) {
        try {
            covidReportRepository.save(covidReport);
            String result = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(covidReport);
            logger.info(String.format("Create covid report %s ", result));
            return result;
        }
        catch (JsonProcessingException e){
            logger.error("Exception occur ", e);
            return e.toString();
        }
    }

    @RequestMapping(value="", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(code = HttpStatus.NO_CONTENT, reason = "OK")
    public String updateCovidReport(@RequestBody CovidReport covidReport) {
        if (Objects.isNull(covidReport.getId())){
            logger.warn("Need to define id");
            return "Need to define id";
        }
        else if (!covidReportRepository.existsById(covidReport.getId())){
            logger.warn(String.format("The covid report with id %s is not exist!", covidReport.getId()));
            return String.format("The covid report with id %s is not exist!", covidReport.getId());
        }
        else {
            try {
                covidReportRepository.save(covidReport);
                String result = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(covidReport);
                logger.info(String.format("Update covid report %s", result));
                return result;
            }
            catch (JsonProcessingException e){
                logger.error("Exception occur ", e);
                return e.toString();
            }
        }
    }

    @RequestMapping(value="", method = RequestMethod.DELETE)
    public @ResponseBody String deleteCovidReport (@RequestParam Long id) {
        Optional<CovidReport> covidReportOptional = covidReportRepository.findById(id);
        if (covidReportOptional.isPresent()) {
            covidReportRepository.delete(covidReportOptional.get());
            logger.info(String.format("Covid report %d delete successfully", id));
            return String.format("Covid report %s delete successfully", covidReportOptional.get().getId());
        } else {
            logger.warn(String.format("Covid report %d doesn't exist", id));
            return String.format("Covid report %d doesn't exist", id);
        }
    }
}
