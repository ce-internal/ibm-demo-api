package ibm.com.ibmdemoapi.entity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "covid_report")
public class CovidReport {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Temporal(TemporalType.DATE)
    private Date date;
    private String city;
    private Integer population;

    //Bottle
    private Integer dailyVaccinationsBottle;
    private Integer totalVaccinationsBottle;
    private String percentageBottle;
    private Integer deliveryBottle;
    private Integer remainingBottle;
    private String remainingPercentageBottle;

    //AZ
    private Integer dailyVaccinationsAz;
    private Integer totalVaccinationsAz;
    private Float percentageAz;
    private Integer deliveryAz;
    private Integer remainingAz;
    private Float remainingPercentageAz;

    //Moderna
    private Integer dailyVaccinationsModerna;
    private Integer totalVaccinationsModerna;
    private Float percentageModerna;
    private Integer deliveryModerna;
    private Integer remainingModerna;
    private Float remainingPercentageModerna;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getPopulation() {
        return population;
    }

    public void setPopulation(Integer population) {
        this.population = population;
    }

    public Integer getDailyVaccinationsBottle() {
        return dailyVaccinationsBottle;
    }

    public void setDailyVaccinationsBottle(Integer dailyVaccinationsBottle) {
        this.dailyVaccinationsBottle = dailyVaccinationsBottle;
    }

    public Integer getTotalVaccinationsBottle() {
        return totalVaccinationsBottle;
    }

    public void setTotalVaccinationsBottle(Integer totalVaccinationsBottle) {
        this.totalVaccinationsBottle = totalVaccinationsBottle;
    }

    public String getPercentageBottle() {
        return percentageBottle;
    }

    public void setPercentageBottle(String percentageBottle) {
        this.percentageBottle = percentageBottle;
    }

    public Integer getDeliveryBottle() {
        return deliveryBottle;
    }

    public void setDeliveryBottle(Integer deliveryBottle) {
        this.deliveryBottle = deliveryBottle;
    }

    public Integer getRemainingBottle() {
        return remainingBottle;
    }

    public void setRemainingBottle(Integer remainingBottle) {
        this.remainingBottle = remainingBottle;
    }

    public String getRemainingPercentageBottle() {
        return remainingPercentageBottle;
    }

    public void setRemainingPercentageBottle(String remainingPercentageBottle) {
        this.remainingPercentageBottle = remainingPercentageBottle;
    }

    public Integer getDailyVaccinationsAz() {
        return dailyVaccinationsAz;
    }

    public void setDailyVaccinationsAz(Integer dailyVaccinationsAz) {
        this.dailyVaccinationsAz = dailyVaccinationsAz;
    }

    public Integer getTotalVaccinationsAz() {
        return totalVaccinationsAz;
    }

    public void setTotalVaccinationsAz(Integer totalVaccinationsAz) {
        this.totalVaccinationsAz = totalVaccinationsAz;
    }

    public Float getPercentageAz() {
        return percentageAz;
    }

    public void setPercentageAz(Float percentageAz) {
        this.percentageAz = percentageAz;
    }

    public Integer getDeliveryAz() {
        return deliveryAz;
    }

    public void setDeliveryAz(Integer deliveryAz) {
        this.deliveryAz = deliveryAz;
    }

    public Integer getRemainingAz() {
        return remainingAz;
    }

    public void setRemainingAz(Integer remainingAz) {
        this.remainingAz = remainingAz;
    }

    public Float getRemainingPercentageAz() {
        return remainingPercentageAz;
    }

    public void setRemainingPercentageAz(Float remainingPercentageAz) {
        this.remainingPercentageAz = remainingPercentageAz;
    }

    public Integer getDailyVaccinationsModerna() {
        return dailyVaccinationsModerna;
    }

    public void setDailyVaccinationsModerna(Integer dailyVaccinationsModerna) {
        this.dailyVaccinationsModerna = dailyVaccinationsModerna;
    }

    public Integer getTotalVaccinationsModerna() {
        return totalVaccinationsModerna;
    }

    public void setTotalVaccinationsModerna(Integer totalVaccinationsModerna) {
        this.totalVaccinationsModerna = totalVaccinationsModerna;
    }

    public Float getPercentageModerna() {
        return percentageModerna;
    }

    public void setPercentageModerna(Float percentageModerna) {
        this.percentageModerna = percentageModerna;
    }

    public Integer getDeliveryModerna() {
        return deliveryModerna;
    }

    public void setDeliveryModerna(Integer deliveryModerna) {
        this.deliveryModerna = deliveryModerna;
    }

    public Integer getRemainingModerna() {
        return remainingModerna;
    }

    public void setRemainingModerna(Integer remainingModerna) {
        this.remainingModerna = remainingModerna;
    }

    public Float getRemainingPercentageModerna() {
        return remainingPercentageModerna;
    }

    public void setRemainingPercentageModerna(Float remainingPercentageModerna) {
        this.remainingPercentageModerna = remainingPercentageModerna;
    }
}
